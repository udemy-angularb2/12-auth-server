const { response } = require("express");
const jwt = require('jsonwebtoken');

const validarJWT = (req, resp = response, next) => {

    const token = req.header('Authorization');

    if ( !token ) {
        return resp.status(401).json({
            message: 'token is required',
            status: 'client error'
        });
    } else {
        try {
            const {uuid, name} =  jwt.verify(token, process.env.SECRET_JWT_SEED);
            req.uuid = uuid;
            req.name = name;
        } catch (error) {
            return resp.status(401).json({
                message: 'token is invalid',
                status: 'client error'
            });
        }
    }
    next();
}

module.exports = {
    validarJWT
}