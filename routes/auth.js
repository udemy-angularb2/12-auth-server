const { Router } = require('express');
const { check } = require('express-validator');
const { crearUsuario, loginUsuario, renewToken } = require('../controllers/auth');
const { validarCampos } = require('../middlewares/validar-campos');
const { validarJWT } = require('../middlewares/validar-token');

const router = Router();

//crear un nuevo usuario
router.post( '/new', [
    check('name', 'name is required').not().isEmpty(),
    check('email', 'email is required').isEmail(),
    check('password', 'password is required').isLength({ min: 6 }),
    validarCampos
], crearUsuario);

//login de usuario
router.post( '/', [
    check('email', 'email is required').isEmail(),
    check('password', 'password is required').isLength({ min: 6 }),
    validarCampos
], loginUsuario);

//renovar token
router.get( '/renew', validarJWT, renewToken);

module.exports = router;