
const express = require("express");
const cors = require('cors');
const { dbConnection } = require("./database/config-db");
require('dotenv').config();

//console.log( process.env );

//crear servidor
const app = express();

//DB CONNECTION
dbConnection();

//directorio publico
app.use( express.static('public') )

//cors
app.use( cors() );

//lectura y parseo del body
app.use( express.json() );

//rutas
app.use('/api/auth', require('./routes/auth'));

//server deploy
app.listen( process.env.PORT, () => {
    console.log(`servidor corriendo en puerto: ${process.env.PORT}`);
});