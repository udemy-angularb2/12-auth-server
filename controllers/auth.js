const { response } = require('express');
const Usuario = require('../models/Usuario');
const bcrypt = require('bcryptjs');
const { generateJWT } = require('../helpers/jwebtoken');


const crearUsuario = async(req, resp = response) => {
    //console.log( req.body );
    const { name, email, password } = req.body;

    try {
        //verificar el email
        const usuario = await Usuario.findOne({ email });

        if( usuario ) {
            return resp.status(400).json({
                message: 'the email is already registered',
                status: 'client error'
            });
        }
        //crear usuario de modelo
        const dbUser = new Usuario( req.body );
        //hashear la cobtraseña
        const salt = bcrypt.genSaltSync();
        dbUser.password = bcrypt.hashSync(password, salt);
        //generar el jwt
        const token = await generateJWT(dbUser.id, name);
        //save usuario
        await dbUser.save();
        //generar response success
        return resp.status(201).json({
            name,
            uuid: dbUser.id,
            status: 'created success',
            token
        });
        
    } catch (error) {
        console.log( error );
        return resp.status(500).json({
            message: 'please contact the administrator',
            status: 'server error'
        });
    }
}

const loginUsuario = async(req, resp = response) => {
    const { email, password } = req.body;
    try {
        const dbuser = await Usuario.findOne({ email });
        if ( !dbuser ) {
            return resp.status(400).json({
                message: 'the email not exit',
                status: 'client error'
            });
        }
        const validPassword = bcrypt.compareSync(password, dbuser.password);
        if ( !validPassword ) {
            return resp.status(400).json({
                message: 'password invalid',
                status: 'client error'
            });
        }
        //generar jwt
        const token = await generateJWT( dbuser.id, dbuser.name );
        //response service
        return resp.json({
            uuid: dbuser.id,
            name: dbuser.name,
            token,
            status: 'success'
        });
    } catch (error) {
        console.log(error);
        return resp.status(500).json({
            message: 'please contact the administrator',
            status: 'server error'
        });
    }
}

const renewToken = async(req, resp = response) => {

    const { uuid, name } = req;

    const token = await generateJWT( uuid, name );

    return resp.json({
        name,
        uuid,
        token,
        status: 'success'
    });
}


module.exports = {
    loginUsuario,
    crearUsuario,
    renewToken
}