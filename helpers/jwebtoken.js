const jwt = require('jsonwebtoken');

const generateJWT = (uuid, name) => {

    const pyload = { uuid, name };

    return new Promise( (resolve, reject) => {
        jwt.sign(pyload, process.env.SECRET_JWT_SEED, {
            expiresIn: '2h'
        }, (err, token) => {
            if ( err ){
                console.log(err);
                reject(err);
            } else {
                resolve(token);
            }
        });
    });
}

module.exports = {
    generateJWT
}